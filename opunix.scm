(use-modules
  (gnu home)
  (gnu packages)
  (gnu packages admin)
  (gnu services)
  (gnu home services shepherd)
  (guix gexp)
  (gnu home services shells)
  (gnu home services))

(home-environment
  (packages
    (map specification->package 
	(list 
		"shepherd"
		"emacs" 
		"htop" 
		"neofetch" 
		"emacs-diminish" 
		"emacs-guix" 
		"emacs-paredit" 
		"emacs-which-key" 
		"emacs-highlight-indent-guides" 
		"font-terminus" 
		"font-google-roboto" 
		"font-fira-code" 
		"fontconfig" 
		"xrandr" 
		"git" 
		"zsh")))
  (services
   (list (service
          home-zsh-service-type
          (home-zsh-configuration
	   (zprofile
            (list (local-file
                   "/home/opunix/.home/zsh/.zprofile"
                   "zprofile")))
           (zshrc
            (list (local-file
                   "/home/opunix/.home/zsh/.zshrc"
                   "zshrc")))))
	 (service
	  home-shepherd-service-type
	  (home-shepherd-configuration
	   (auto-start? #t))))))
