(use-modules (gnu home)
	     (gnu packages bash)
	     (gnu packages emacs)
	     (gnu packages admin)
	     (gnu home services shepherd)
	     (gnu home services))

(define simple-services
  (home-environment
   (packages `(,bash ,emacs))
   (services
    (list (service home-shepherd-service-type
	   (home-shepherd-configuration
	    (shepherd shepherd)
	    (services '())
	    (auto-start? #t)))))))

simple-services
