(use-modules
  (gnu home)
  (gnu packages)
  (gnu services)
  (guix gexp)
  (gnu home services shells)
  (gnu home services))

(home-environment
  (packages
    (map specification->package 
	(list 
		"vim" 
		"emacs" 
		"htop" 
		"neofetch" 
		"alacritty" 
		"fzf" 
		"firefox" 
		"tmux" 
		"emacs-diminish" 
		"emacs-guix" 
		"emacs-paredit" 
		"emacs-which-key" 
		"emacs-direnv" 
		"emacs-highlight-indent-guides" 
		"font-terminus" 
		"font-google-roboto" 
		"font-fira-code" 
		"fontconfig" 
		"xrandr" 
		"git" 
		"direnv")))
  (services
    (list (service
            home-bash-service-type
            (home-bash-configuration
	      (guix-defaults? #t)
	      (bash-profile
                (list (local-file
                        "~/.home/bash/.bash_profile"
                        "bash_profile")))
              (bashrc
                (list (local-file
                        "~/.home/bash/.bashrc"
                        "bashrc")))))
	  (simple-service 'i3-config
			  home-files-service-type
			  (list `("config/i3/config"
				  ,(local-file "./i3/config"))))
	  (simple-service 'emacs-confg
			  home-files-service-type
			  (list `("emacs.d/init.el"
				  ,(local-file "emacs/init.el"))
				`("emacs.d/early-init.el"
				  ,(local-file "emacs/early-init.el"))))
	  (simple-service 'direnv-config
			  home-files-service-type
			  (list `("direnvrc"
				  ,(local-file "direnv/direnvrc")))))))
