(use-modules (gnu home)
	     (gnu packages bash)
	     (gnu packages emacs))

(define simple
  (home-environment
   (packages `(,bash ,emacs))))

simple
